package com.epam.controller;

import org.hibernate.Session;

import java.sql.SQLException;

public interface DistrictController extends GeneralController {
    void findByCity();
    void findByDistrict();
}
