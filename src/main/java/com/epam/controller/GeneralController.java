package com.epam.controller;

import org.hibernate.Session;

import java.sql.SQLException;

public interface GeneralController {
    void delete();
    void create();
    void update();
    void select();
    void findByID();
    String getTableName();
}
