package com.epam.controller.impl;

import com.epam.controller.DistrictController;
import com.epam.model.location.CityEntity;
import com.epam.model.location.DistrictEntity;
import com.epam.model.location.DistrictPK;
import com.epam.service.CityService;
import com.epam.service.DistrictService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Scanner;

public class DistrictControllerImpl implements DistrictController {
    private static final Logger logger = LogManager.getLogger(DistrictControllerImpl.class.getName());
    private static Scanner scanner = new Scanner(System.in);
    private DistrictService districtService;

    public DistrictControllerImpl() {
        districtService = new DistrictService();
    }

    @Override
    public void select(){
        System.out.println("Table: District");
        List<DistrictEntity> districts = districtService.findAll();
        logger.info(String.format("|%-45s|%-45s|\n", "City", "District"));
        for (DistrictEntity entity : districts) {
            logger.info(entity);
        }
    }

    @Override
    public void create() {
        DistrictEntity districtEntity= createDistrictEntity();
        districtService.create(districtEntity);
    }

    @Override
    public void delete(){
        districtService.delete(createDistrictPK());
    }

    @Override
    public void update(){
        DistrictEntity oldEntity = createDistrictEntity();
        DistrictEntity newEntity = createDistrictEntity();
        districtService.update(oldEntity, newEntity);
    }

    @Override
    public void findByID(){
        logger.info(districtService.findById(createDistrictPK()));//shows
        // only in debug mode :( idk why
    }

    @Override
    public void findByCity(){
        logger.info("Input city_name to select from District: ");
        String cityName = scanner.nextLine();
        CityEntity city = new CityEntity();
        city.setCityName(cityName);
        for (DistrictEntity entity : districtService.findByCity(city)) {
            logger.info(entity);
        }
    }

    @Override
    public void findByDistrict(){
        logger.info("Input district_name to select from District: ");
        String districtName = scanner.nextLine();
        for (DistrictEntity entity : districtService.findByDistrict(districtName)) {
            logger.info(entity);
        }
    }

    @Override
    public String getTableName() {
        return "District";
    }

    private DistrictEntity createDistrictEntity(){
        DistrictEntity districtEntity = new DistrictEntity();
        logger.info("Input city_name for District: ");
        String cityName = scanner.nextLine();
        CityEntity cityEntity = new CityEntity();
        cityEntity.setCityName(cityName);
        logger.info("Input district_name for District: ");
        String districtName = scanner.nextLine();
        districtEntity.setCityByCityName(cityEntity);
        districtEntity.setDistrictName(districtName);
        return districtEntity;
    }

    private DistrictPK createDistrictPK(){
        logger.info("Input city_name for District: ");
        String cityName = scanner.nextLine();
        CityEntity cityEntity = new CityService().findById(cityName);
        logger.info("Input district_name for District: ");
        String districtName = scanner.nextLine();
        DistrictPK districtPK = new DistrictPK(districtName, cityEntity);
        return districtPK;
    }
}
