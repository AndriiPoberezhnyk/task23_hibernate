package com.epam.controller.impl;

import com.epam.controller.CityController;
import com.epam.model.location.CityEntity;
import com.epam.service.CityService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Scanner;

public class CityControllerImpl implements CityController {
    private static final Logger logger = LogManager.getLogger(CityControllerImpl.class.getName());
    private static Scanner scanner = new Scanner(System.in);
    private CityService cityService;

    public CityControllerImpl() {
        cityService = new CityService();
    }

    @Override
    public void delete() {
        logger.info("Input city_name for City ");
        String cityName = scanner.nextLine();
        cityService.delete(cityName);
    }

    @Override
    public void create() {
        cityService.create(createCityEntity());
    }

    @Override
    public void update() {
        logger.info("Old city value: ");
        CityEntity oldEntity = createCityEntity();
        logger.info("New city value: ");
        CityEntity newEntity = createCityEntity();
        cityService.update(oldEntity, newEntity);
    }

    @Override
    public void select() {
        logger.info("Table: City");
        List<CityEntity> cities = cityService.findAll();
        logger.info(String.format("|%-45s|\n", "City"));
        for (CityEntity entity : cities) {
            logger.info(entity);
        }
    }

    @Override
    public void findByID() {
        logger.info("Input city_name for City: ");
        String cityName = scanner.nextLine();
        logger.info(cityService.findById(cityName));
    }

    @Override
    public String getTableName() {
        return "City";
    }

    private CityEntity createCityEntity() {
        logger.info("Input city_name for City: ");
        String cityName = scanner.nextLine();
        CityEntity entity = new CityEntity();
        entity.setCityName(cityName);
        return entity;
    }
}
