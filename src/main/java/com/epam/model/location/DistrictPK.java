package com.epam.model.location;

import lombok.Getter;

import java.io.Serializable;
import java.util.Objects;

@Getter
public class DistrictPK implements Serializable {
    private String districtName;
    private CityEntity cityByCityName;

    public DistrictPK() {
    }

    public DistrictPK(String districtName, CityEntity cityByCityName) {
        this.districtName = districtName;
        this.cityByCityName = cityByCityName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DistrictPK that = (DistrictPK) o;
        return districtName.equals(that.districtName) &&
                cityByCityName.equals(that.cityByCityName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(districtName, cityByCityName);
    }
}
