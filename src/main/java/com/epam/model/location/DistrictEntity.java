package com.epam.model.location;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "district", schema = "booking")
@IdClass(DistrictPK.class)
public class DistrictEntity {
    @Id
    @Column(name = "district_name")
    private String districtName;
    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "city_name", referencedColumnName = "city_name",
            nullable = false)
    private CityEntity cityByCityName;

    @Override
    public String toString() {
        return String.format("|%-45s|%-45s|", cityByCityName.getCityName()
                , districtName);
    }
}
