package com.epam.view.table_views;

import com.epam.controller.GeneralController;
import com.epam.view.Printable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class BasicTableView {
    private static final Logger logger = LogManager.getLogger(BasicTableView.class.getName());
    private GeneralController controller;
    protected Map<String, String> menu;
    protected Map<String, Printable> methodsMenu;
    protected static Scanner scanner = new Scanner(System.in);

    public BasicTableView(GeneralController controller) {
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();
        this.controller = controller;
    }

    public void run() {
        String keyMenu;
        do {
            generateMainMenu();
            outputMenu();
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (SQLException e){
                logger.error("ErrorCode = " + e.getErrorCode()
                                +", SQLState = " + e.getSQLState()
                                    + ", " + e.getLocalizedMessage());
            } catch (Exception ignored) {
            }
        } while (!keyMenu.equals("Q"));
    }


    protected void generateMainMenu() {
        menu.clear();
        menu.put("1", "  1 - Create for " + controller.getTableName());
        menu.put("2", "  2 - Update " + controller.getTableName());
        menu.put("3", "  3 - Delete from " + controller.getTableName());
        menu.put("4", "  4 - Select all from " + controller.getTableName());
        menu.put("5", "  5 - Find " + controller.getTableName() + " by ID");

        methodsMenu.clear();
        methodsMenu.put("1", this::create);
        methodsMenu.put("2", this::update);
        methodsMenu.put("3", this::delete);
        methodsMenu.put("4", this::select);
        methodsMenu.put("5", this::findByID);
    }

    protected void create() throws SQLException {
        controller.create();
    }

    protected void update() throws SQLException {
        controller.update();
    }

    protected void delete() throws SQLException {
        controller.delete();
    }

    protected void select() throws SQLException {
        controller.select();
    }

    protected void findByID() {
        controller.findByID();
    }

    private void outputMenu() {
        menu.put("Q", "  Q - Quit");
        logger.trace("Table: " + controller.getTableName());
        for (String option : menu.values()) {
            logger.trace(option);
        }
        logger.trace(">>> ");
    }
}
