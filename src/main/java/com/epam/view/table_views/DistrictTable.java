package com.epam.view.table_views;

import com.epam.controller.DistrictController;

import java.sql.SQLException;

public class DistrictTable extends BasicTableView {
    private DistrictController controller;

    public DistrictTable(DistrictController controller) {
        super(controller);
        this.controller = controller;
    }

    @Override
    protected void generateMainMenu() {
        super.generateMainMenu();
        menu.put("6", "  6 - Find " + controller.getTableName() + " by city");
        menu.put("7", "  7 - Find " + controller.getTableName() +
                " by " +
                "district");

        methodsMenu.put("6", this::findByCity);
        methodsMenu.put("7", this::findByDistrict);
    }

    private void findByCity() throws SQLException {
        controller.findByCity();

    }

    private void findByDistrict() throws SQLException {
        controller.findByDistrict();
    }
}
