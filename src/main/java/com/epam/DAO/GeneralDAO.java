package com.epam.DAO;

import java.sql.SQLException;
import java.util.List;

public interface GeneralDAO<T, ID> {
    List<T> findAll();

    T findById(ID id);

    void create(T entity) ;

    void update(T oldEntity, T newEntity) ;

    void delete(ID id);
}
