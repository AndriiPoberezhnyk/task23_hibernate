package com.epam.DAO.locationDAO.impl;

import com.epam.DAO.locationDAO.CityDAO;
import com.epam.model.location.CityEntity;
import com.epam.persistant.ConnectionManager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class CityDaoImpl implements CityDAO {
    private static final Logger logger = LogManager.getLogger(CityDaoImpl.class.getName());

    @Override
    public void create(CityEntity entity){
        final Session session = ConnectionManager.getSession();
        session.beginTransaction();
        session.save(entity);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public void update(CityEntity oldEntity, CityEntity newEntity) {
        final Session session = ConnectionManager.getSession();
        CityEntity cityEntity = session.load(CityEntity.class,
                oldEntity.getCityName());
        int result;
        if (cityEntity != null) {
            session.beginTransaction();
            Query query = session.createQuery ("update CityEntity set " +
                    "cityName=:code1 where cityName = :code2");
            query.setParameter("code1", newEntity.getCityName());
            query.setParameter("code2", cityEntity.getCityName());
            result = query.executeUpdate();
            session.getTransaction().commit();
            logger.info("updated city: " + result);
        } else logger.info("There is no the city");
        session.close();
    }

    @Override
    public void delete(String cityName) {
        final Session session = ConnectionManager.getSession();
        CityEntity cityEntity = session.load(CityEntity.class, cityName);
        if (cityEntity != null) {
            session.beginTransaction();
            session.delete(cityEntity);
            session.getTransaction().commit();
        } else logger.info("There is no the city");
        session.close();
    }

    @Override
    public List<CityEntity> findAll() {
        final Session session = ConnectionManager.getSession();
        Query query = session.createQuery("from CityEntity");
        List<CityEntity> cities = query.list();
        session.close();
        return cities;
    }

    @Override
    public CityEntity findById(String cityName)  {
        final Session session = ConnectionManager.getSession();
        session.beginTransaction();
        Query query = session.createQuery("from CityEntity where " +
                "cityName =:code1");
        query.setParameter("code1",cityName);
        List<CityEntity> cities = query.getResultList();
        session.getTransaction().commit();
        session.close();
        if (cities.size()==0){
            logger.info("There is no such city");
            return null;
        }else return cities.get(0);
    }

}
