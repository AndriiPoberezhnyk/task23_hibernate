package com.epam.DAO.locationDAO.impl;

import com.epam.DAO.locationDAO.DistrictDAO;
import com.epam.model.location.CityEntity;
import com.epam.model.location.DistrictEntity;
import com.epam.model.location.DistrictPK;
import com.epam.persistant.ConnectionManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class DistrictDaoImpl implements DistrictDAO {
    private static final Logger logger = LogManager.getLogger(DistrictDaoImpl.class.getName());


    @Override
    public List<DistrictEntity> findByDistrict(String districtName){
        final Session session = ConnectionManager.getSession();
        session.beginTransaction();
        Query query = session.createQuery("from DistrictEntity where " +
                "districtName =:code1");
        query.setParameter("code1", districtName);
        List<DistrictEntity> districts =  query.getResultList();
        session.getTransaction().commit();
        session.close();
        return districts;
    }

    @Override
    public List<DistrictEntity> findByCity(CityEntity cityName) {
        final Session session = ConnectionManager.getSession();
        session.beginTransaction();
        Query query = session.createQuery("from DistrictEntity where " +
                "cityByCityName.cityName =:code1");
        query.setParameter("code1", cityName.getCityName());
        List<DistrictEntity> districts = query.getResultList();
        session.getTransaction().commit();
        session.close();
        return districts;
    }

    @Override
    public List<DistrictEntity> findAll(){
        final Session session = ConnectionManager.getSession();
        Query query = session.createQuery("from DistrictEntity");
        List<DistrictEntity> districts = query.list();
        session.close();
        return districts;
    }

    @Override
    public DistrictEntity findById(DistrictPK districtPK) {
        final Session session = ConnectionManager.getSession();
        session.beginTransaction();
        Query query = session.createQuery("from DistrictEntity where " +
                "cityByCityName.cityName =:code1 and districtName =:code2");
        query.setParameter("code1", districtPK.getCityByCityName().getCityName());
        query.setParameter("code2", districtPK.getDistrictName());
        List<DistrictEntity> districts = query.list();
        session.getTransaction().commit();
        session.close();
        if (districts.size()==0){
            logger.info("There is no such district");
            return null;
        }else {
            logger.info(districts.get(0)); //shows only in debug mode :(
            return districts.get(0);
        }
    }

    @Override
    public void create(DistrictEntity entity) {
        final Session session = ConnectionManager.getSession();
        session.beginTransaction();
        session.save(entity);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public void update(DistrictEntity oldEntity, DistrictEntity newEntity) {
        final Session session = ConnectionManager.getSession();
        session.beginTransaction();
        Query query = session.createQuery("update DistrictEntity set " +
                "cityByCityName.cityName =:code1, districtName =:code2 where " +
                "cityByCityName.cityName =:code3 and districtName =:code4");
        query.setParameter("code1", newEntity.getCityByCityName().getCityName());
        query.setParameter("code2", newEntity.getDistrictName());
        query.setParameter("code3", oldEntity.getCityByCityName().getCityName());
        query.setParameter("code4", oldEntity.getDistrictName());
        int count = query.executeUpdate();
        logger.info("updated "+ count + " rows");
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public void delete(DistrictPK districtPK) {
        final Session session = ConnectionManager.getSession();
        session.beginTransaction();
        Query query = session.createQuery("from DistrictEntity where " +
                "cityByCityName.cityName =:code1 and districtName =:code2");
        query.setParameter("code1", districtPK.getCityByCityName().getCityName());
        query.setParameter("code2", districtPK.getDistrictName());
        List<DistrictEntity> districts = query.getResultList();
        if (districts.size()==0){
            logger.info("There is no such district");
        }else session.delete(districts.get(0));
        session.getTransaction().commit();
        session.close();
    }
}
