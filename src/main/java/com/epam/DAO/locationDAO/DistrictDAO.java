package com.epam.DAO.locationDAO;

import com.epam.DAO.GeneralDAO;
import com.epam.model.location.CityEntity;
import com.epam.model.location.DistrictEntity;
import com.epam.model.location.DistrictPK;

import java.util.List;

public interface DistrictDAO extends GeneralDAO<DistrictEntity, DistrictPK> {
    List<DistrictEntity> findByDistrict(String name);

    List<DistrictEntity> findByCity(CityEntity cityName);
}
