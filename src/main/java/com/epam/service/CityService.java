package com.epam.service;

import com.epam.DAO.locationDAO.impl.CityDaoImpl;
import com.epam.model.location.CityEntity;

import java.sql.SQLException;
import java.util.List;

public class CityService {
    private CityDaoImpl cityDao;

    public CityService() {
        cityDao = new CityDaoImpl();
    }

    public List<CityEntity> findAll(){
        return cityDao.findAll();
    }

    public CityEntity findById(String id){
        return cityDao.findById(id);
    }

    public void create(CityEntity entity){
        cityDao.create(entity);
    }

    public void update(CityEntity oldEntity, CityEntity newEntity){
        cityDao.update(oldEntity, newEntity);
    }

    public void delete(String cityName) {
        cityDao.delete(cityName);
    }
}
