package com.epam.service;

import com.epam.DAO.locationDAO.impl.DistrictDaoImpl;
import com.epam.model.location.CityEntity;
import com.epam.model.location.DistrictEntity;
import com.epam.model.location.DistrictPK;

import java.util.List;

public class DistrictService {
    private DistrictDaoImpl districtDao;

    public DistrictService() {
        districtDao = new DistrictDaoImpl();
    }

    public List<DistrictEntity> findAll(){
        return districtDao.findAll();
    }

    public DistrictEntity findById(DistrictPK id){
        return districtDao.findById(id);
    }

    public void create(DistrictEntity entity){
        districtDao.create(entity);
    }

    public void update(DistrictEntity oldEntity, DistrictEntity newEntity){
        districtDao.update(oldEntity, newEntity);
    }

    public void delete(DistrictPK districtPK) {
        districtDao.delete(districtPK);
    }

    public List<DistrictEntity> findByCity(CityEntity cityName){
        return districtDao.findByCity(cityName);
    }

    public List<DistrictEntity> findByDistrict(String districtName){
        return districtDao.findByDistrict(districtName);
    }
}
